import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
import torchvision
import torchvision.transforms as transforms
import pandas as pd
import numpy as np
import os
import cv2
from torch.utils.data import Dataset
from torch.utils.data import DataLoader
from model import Model, train
from dataloader import dataloaderMain, create_dataset, read_dataset

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
csv_file = "data/database.csv"
root_dir = "data/"
batch_size = 200
shuffle = True
if torch.cuda.is_available():
    print("GPU is used")

class Data(Dataset):
    def __init__(self, csv_file, root_dir, transform=None):
        """ Load the dataset"""

        self.anotation = dataloaderMain()
        print(type(self.anotation))
        print("loaded from csv")
        self.anotation = np.array(self.anotation)
        self.labels    = self.anotation[:,-1]
        self.anotation = np.delete(self.anotation,-1,axis=1)
        self.anotation = self.anotation.reshape(20000,120,320)
        self.anotation = np.stack((self.anotation,)*1,axis=-1)
        self.anotation = self.anotation.reshape(20000,1,120,320)
#        self.anotation = self.anotation.transpose()
        self.anotation = torch.from_numpy(self.anotation).float()
        self.labels = torch.from_numpy(self.labels).float()
        self.root_dir = root_dir
        self.transform = transform
        print("Clear")

    def __len__(self):
        """ returns the length of the first dimension """
        return len(self.anotation) #20 000 images 4096 each

    def __getitem__(self,index):
        """ Data extraction from tensor """
        return self.anotation[index,:], self.labels[index]

    def __padding__(self,data,fshape,imshape):
        """ Adds padding to 64x64 matrix to make it 224x224 """

        ret = np.zeros((fshape,imshape,imshape),dtype="float64")
        for i in range(fshape):
            padded = np.pad(data[i],80,pad_with)
            ret[i] = padded
        return ret

    def __slice_update(self, data,fshape, imshape):
        ret = np.zeros((fshape, imshape, imshape))
        dshape = data.shape[1]
        lower = (imshape) // 2 - (dshape // 2)
        upper = (imshape // 2) + (dshape // 2)
        for i in range(fshape):
            ret[i,lower:upper, lower:upper] = data[i]
        return ret

def Dataset(csv_file,root,batch_size,shuffle,):

    """ Create three separate dataloaders which we can use to train, validate and test the network """

    dataset = Data(csv_file, root_dir=root,transform=transforms.ToTensor())
    train, test = torch.utils.data.random_split(dataset, [14000,6000])
    valid, test = torch.utils.data.random_split(test, [3000,3000])

    train_loader = DataLoader(dataset=train, batch_size=batch_size, shuffle=shuffle)
    valid_loader = DataLoader(dataset=valid, batch_size=batch_size, shuffle=shuffle)

    test_loader  = DataLoader(dataset=test, batch_size=batch_size, shuffle=shuffle)

    return train_loader, valid_loader, test_loader

def pad_with(vector,pad_width,iaxis,kwargs):
    """ zero padding procedure for matrixes with """
    pad_value = kwargs.get('padder', 0)
    vector[:pad_width[0]] = pad_value
    vector[-pad_width[1]:] = pad_value


def main():

    train,valid,test = Dataset(csv_file, root_dir, batch_size, shuffle)
    print("The data has been loaded")

    #print(train.size())
    """ create the model and define the optimizer and the loss function """
    network = Model()
    network = network.to(device)
    print("Model created")

    optimizer = torch.optim.SGD(network.parameters(),lr=0.001,momentum=0.9)
    criterion = nn.CrossEntropyLoss()

    """ Train the network and save its training and validating accuracy and loss """
    print("Training has begun")
    model, train_accuracy, train_loss, valid_accuracy, valid_loss = train(network,
                                                                        train,
                                                                        valid,
                                                                        learning_rate=0.001,
                                                                        num_epochs=100,
                                                                        optimizer=optimizer,
                                                                        criterion=criterion)
    print("HEYLYA")
    """ save the model """

    torch.save(model,"model/lazy.pth")
    torch.save(model.state_dict(),"model/model.pth")

    np.savetxt('data/train_accuracy.csv', train_accuracy, delimiter=',')
    np.savetxt('data/train_loss.csv', train_loss, delimiter=',')
    np.savetxt('data/valid_accuracy.csv', valid_accuracy, delimiter=',')
    np.savetxt('data/valid_loss.csv', valid_loss, delimiter=',')

    targets, outputs = model.test(test)

    outputs = np.array(outputs)
    np.savetxt('data/test_output.csv', outputs, delimiter=',')

    targets = np.array(targets)
    np.savetxt('data/test_labels.csv', targets, delimiter=',')

main()
