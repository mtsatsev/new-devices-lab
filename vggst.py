import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
import torchvision
import torchvision.transforms as transforms
import numpy as np

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

class Model(nn.Module):
    def __init__(self,input_size=3, hidden_size=64,n_classes=10):
        """ Define our model, its a very complex model of type VGG16 """
        super(Model, self).__init__()
        self.second_hidden = hidden_size*2
        self.third_hidden  = self.second_hidden*2
        self.fourth_hidden = self.third_hidden*2

        self.sequentialConv = nn.Sequential(
        nn.Conv2d(in_channels=input_size, out_channels=hidden_size,kernel_size=3,padding=1,stride=1),
        nn.Conv2d(in_channels=hidden_size,out_channels=hidden_size,kernel_size=3,padding=1,stride=1),
        nn.MaxPool2d(kernel_size=2,stride=2),
        nn.Conv2d(in_channels=hidden_size,out_channels=self.second_hidden, kernel_size=3, padding=1,stride=1),
        nn.Conv2d(in_channels=self.second_hidden,out_channels=self.second_hidden, kernel_size=3,padding=1,stride=1),

        nn.MaxPool2d(kernel_size=2, stride=2),
        nn.Conv2d(in_channels=self.second_hidden,out_channels=self.third_hidden, kernel_size=3, padding=1, stride=1),
        nn.Conv2d(in_channels=self.third_hidden,out_channels=self.third_hidden,kernel_size=3,padding=1, stride=1),
        nn.Conv2d(in_channels=self.third_hidden,out_channels=self.third_hidden,kernel_size=3,padding=1, stride=1),

        nn.MaxPool2d(kernel_size=2, stride=2),
        nn.Conv2d(in_channels=self.third_hidden,out_channels=self.fourth_hidden,kernel_size=3,padding=1, stride=1),
        nn.Conv2d(in_channels=self.fourth_hidden,out_channels=self.fourth_hidden,kernel_size=1,padding=1, stride=1),
        nn.Conv2d(in_channels=self.fourth_hidden,out_channels=self.fourth_hidden,kernel_size=3,padding=1, stride=1),

        nn.MaxPool2d(kernel_size=2, stride=2),
        nn.Conv2d(in_channels=self.fourth_hidden,out_channels=self.fourth_hidden,kernel_size=3,padding=1, stride=1),
        nn.Conv2d(in_channels=self.fourth_hidden,out_channels=self.fourth_hidden,kernel_size=3,padding=1, stride=1),
        nn.Conv2d(in_channels=self.fourth_hidden,out_channels=self.fourth_hidden,kernel_size=3,padding=1, stride=1),

        nn.MaxPool2d(kernel_size=2, stride=2)
        )

        self.sequntialForw = nn.Sequential(
        nn.Linear(self.fourth_hidden*2*2,4096),
        nn.ReLU(),
        nn.Dropout(p=0.5),
        nn.Linear(4096,4096),
        nn.ReLU(),
        nn.Dropout(p=0.5),
        nn.Linear(4096,n_classes),
        nn.Softmax()
        )

    def forward(self, x):
        """ The forward pass of our model """
        x = self.sequentialConv(x.float())
        x = x.reshape(x.shape[0],-1)
        x = self.sequntialForw(x.float())
        return x

    def predict(self, images):
        """ Predict a single image """
        x = self.forward(images)
        x = torch.argmax(x)
        return x.item()
    '''
    def train(self,train_data,valid_data,learning_rate,num_epochs,optimizer,criterion):
        """ Training procedure of the model together with accuracy and loss for both data sets """
        train_loss = np.zeros(num_epochs)
        valid_loss = np.zeros(num_epochs)
        train_accuracy = np.zeros(num_epochs)
        valid_accuracy = np.zeros(num_epochs)

        """begin training"""
        for epoch in range(num_epochs):
            train_losses = []
            train_correct= 0
            total_items  = 0

            valid_losses = []
            valid_correct = 0

            for images,labels in train_data:

                images = images.float()
                labels = labels.long()
                optimizer.zero_grad()

                """add to GPU hopefully"""
                images = images.to(device)
                labels = labels.to(device)

                """Forward pass"""
                outputs = self.forward(images)

                loss    = criterion(outputs,labels)

                """Backward pass"""
                loss.backward()
                optimizer.step()

                """staticstics"""
                train_losses.append(loss.item())
                _, predicted = torch.max(outputs.data,1)
                train_correct += (predicted == labels).sum().item()
                total_items += labels.size(0)

            train_loss[epoch] = np.mean(train_losses)
            train_accuracy[epoch] = (1 * train_correct/total_items)

            with torch.no_grad():
                correct_val = 0
                total_val = 0

                for images,labels in valid_data:

                    images = images.float()
                    labels = labels.long()

                    images = images.to(device)
                    labels = labels.to(device)

                    outputs = self.forward(images)
                    loss    = criterion(outputs, labels)

                    valid_losses.append(loss.item())
                    _, predicted = torch.max(outputs.data, 1)

                    correct_val += (predicted == labels).sum().item()
                    total_val   += labels.size(0)

            valid_loss[epoch] = np.mean(valid_losses)
            valid_accuracy[epoch] = (1 * correct_val/total_val)

            print("Epoch: [{},{}], train accuracy: {}, valid accuracy: {}, train loss: {}, valid loss: {}"
            .format(num_epochs,epoch+1,train_accuracy[epoch],valid_accuracy[epoch],train_loss[epoch],valid_accuracy[epoch]))

        return train_accuracy, train_loss, valid_accuracy, valid_loss
        '''
    def test(self,test_data):
        """ Test our model on the testing data set """
        outputs = []
        targets = []
        with torch.no_grad():
            for images,labels in test_data:
                images = images.to(device)
                labels = labels.to(device)

                prediction = self.predict(images)
                outputs.append(prediction.item())
                targets.append(labels.cpu().detach().numpy())

        return targets, outputs
