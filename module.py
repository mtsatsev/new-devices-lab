import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
import torchvision
import torchvision.transforms as transforms
from vggst import Model
import numpy as np
import cv2
import imutils
import argparse
import pandas as pd
from utilityfunctions import filter_colors,find_countours,masked

class Hand():
    def __init__(self,frame,color=(0,0,0),block=False):
        self.fill = [1,-1][block]
        self.hands = cv2.CascadeClassifier("cascades/hand.xml")
        self.palmr = cv2.CascadeClassifier("cascades/rpalm.xml")
        self.right = cv2.CascadeClassifier("cascades/right.xml")
        self.fists = cv2.CascadeClassifier("cascades/fist.xml")
        self.frame = frame
        self.color = color

    def detect_hand(self):
        gray = cv2.cvtColor(self.frame, cv2.COLOR_BGR2GRAY)
        area = 0
        X= Y= W= H = 0
        hands= self.palmr.detectMultiScale(gray,1.1,2)

        if hands is not None:
            for(x, y, w, h) in hands:
                if w * h > area:
                    area = w * h
                    X, Y, W, H = x, y, w, h
            rect = cv2.rectangle(self.frame, (X,Y), (X + W, Y + H),self.color, self.fill)
        """
        if palmr is not None:
            for(x, y, w, h) in palmr:
                if w * h > area:
                    area = w * h
                    X, Y, W, H = x, y, w, h
            rect = cv2.rectangle(self.frame, (X,Y), (X + W, Y + H),self.color, self.fill)
        elif right is not None:
            for(x, y, w, h) in right:
                if w * h > area:
                    area = w * h
                    X, Y, W, H = x, y, w, h
            rect = cv2.rectangle(self.frame, (X,Y), (X + W, Y + H),self.color, self.fill)
        else:
            for(x, y, w, h) in fists:
                if w * h > area:
                    area = w * h
                    X, Y, W, H = x, y, w, h
            rect = cv2.rectangle(self.frame, (X,Y), (X + W, Y + H),self.color, self.fill)
        """
        #My pc is too weak and it gets laggy
        return rect


class Face():
    def __init__(self,frame,color=(0,0,0),block=False):
        self.fill = [1,-1][block]
        self.face_cascade = cv2.CascadeClassifier(cv2.data.haarcascades + "haarcascade_frontalface_default.xml")
        self.frame = frame
        self.color = color

    def detect_face(self):
        gray = cv2.cvtColor(self.frame, cv2.COLOR_BGR2GRAY)
        faces = self.face_cascade.detectMultiScale(gray,1.1,5)
        area = 0
        X= Y= W= H = 0
        for(x, y, w, h) in faces:
            if w * h > area:
                area = w * h
                X, Y, W, H = x, y, w, h
        cv2.rectangle(self.frame, (X,Y), (X + W, Y + H*2),self.color, self.fill)

class Device():
    def __init__(self):
        self.device = 0
        self.capture = cv2.VideoCapture(self.device)
        self.lower_color = np.array([0, 15, 0], dtype=np.uint8)
        self.upper_color = np.array([14, 255, 255], dtype=np.uint8)
        self.kernel = np.ones((3,3),np.uint8)
        self.bluer  = (5,5)

        if not(self.capture.isOpened()):
            print("I failed")
            self.capture.open(self.device)
        else:
            print("Heylya")
    def nothing(self,x):
        pass

    def start(self,model):

        cv2.namedWindow("Trackbars")
        cv2.createTrackbar("L – H", "Trackbars", 0, 179, self.nothing)
        cv2.createTrackbar("L – S", "Trackbars", 0, 255, self.nothing)
        cv2.createTrackbar("L – V", "Trackbars", 0, 255, self.nothing)
        cv2.createTrackbar("U – H", "Trackbars", 179, 179, self.nothing)
        cv2.createTrackbar("U – S", "Trackbars", 255, 255, self.nothing)
        cv2.createTrackbar("U – V", "Trackbars", 255, 255, self.nothing)

        while True:
            ret,frame = self.capture.read()
            frame = cv2.flip(frame,flipCode=1)
            gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
            face = Face(frame,block=False)
            face.detect_face()
            #hand = Hand(frame=frame)
            #rect = hand.detect_hand()

            l_h = cv2.getTrackbarPos("L – H", "Trackbars")
            l_s = cv2.getTrackbarPos("L – S", "Trackbars")
            l_v = cv2.getTrackbarPos("L – V", "Trackbars")
            u_h = cv2.getTrackbarPos("U – H", "Trackbars")
            u_s = cv2.getTrackbarPos("U – S", "Trackbars")
            u_v = cv2.getTrackbarPos("U – V", "Trackbars")

            lower = np.array([l_h,l_s,l_v])
            upper = np.array([u_h,u_s,u_v])
            #mask = masked(frame,lower,upper,self.kernel,self.bluer)
            mask = masked(frame,self.lower_color,self.upper_color,self.kernel,self.bluer)

            target = cv2.resize(gray,(64,64))
            target = np.stack((target,)*3,axis=-1)
            target = target.reshape(1,3,64,64)
            target = torch.from_numpy(target)
            prediction = model.predict(target)
            print(prediction)

            cv2.imshow("hand detector", frame)
            #cv2.imshow("Show two things",frame)
            if cv2.waitKey(1) & 0xFF == ord('q'):
                break
        self.capture.release()
        cv2.destroyAllWindows()

def main():
    model = Model()
    model.load_state_dict(torch.load("model/model.pth",map_location=torch.device('cpu')))
    model.eval()

    device = Device()
    device.start(model)


main()
