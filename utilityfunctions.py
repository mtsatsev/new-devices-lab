from os import *
import numpy as np
import cv2
import pandas as pd
import argparse


def color_image_gray(folder,filename):
    """ Read an image from a folder, color it gray and inver it """
    image = cv2.imread(path.join(folder,filename))
    image2 = cv2.cvtColor(image,cv2.COLOR_RGB2GRAY)
    return ~image2

def convert_image_into_binary_image(img):
    """ Conver a given image into binary so it be read by the computer """
    ret,bw_img = cv2.threshold(img,0,255, (cv2.THRESH_BINARY + cv2.THRESH_OTSU))
    return bw_img

def find_countours(image):
    """ Find the counturs in an image """
    cnts,hierarchy = cv2.findContours(image,cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_NONE)
    return cnts

def filter_colors(image,lower, upper):
    """ Threshold the image to the color bounds """
    hsv = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)
    mask = cv2.inRange(hsv, lower, upper)
    return mask

def get_contours_center(contour):
    """ Find the center of the found contour """
    center = cv2.moments(contour)
    cx = cy = -1
    if(center["m00"] != 0):
        cx = int(center['m10']/ center['m00'])
        cy = int(center['m01']/ center['m00'])
    return cx, cy

def masked(frame,lower,upper,kernel,bluer):
    mask = filter_colors(frame, lower,upper)
    mask = cv2.dilate(mask,kernel,iterations=4)
    mask = cv2.GaussianBlur(mask,bluer,100)
    return mask
